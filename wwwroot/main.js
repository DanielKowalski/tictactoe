(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./app/app.component.css":
/*!*******************************!*\
  !*** ./app/app.component.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div {\r\n  text-align: center;\r\n}\r\nh1 {\r\n  color: red;\r\n}"

/***/ }),

/***/ "./app/app.component.html":
/*!********************************!*\
  !*** ./app/app.component.html ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div style=\"text-align:center\">\n  <h1>\n    Witaj w {{ title }}!\n  </h1>\n</div>\n<div *ngIf=\"!isGame()\">\r\n  <a mat-button (click)=\"playWithPlayer()\">Gorące krzesło</a>\r\n  <a mat-button (click)=\"playWithComputer()\">Z komputerem</a>\r\n</div>\n<div *ngIf=\"isGame()\">\n  <app-game [versus]=\"versus\"></app-game>\n  <br />\n  <a (click)=\"back()\" mat-button>Wróć</a>\n</div>\n"

/***/ }),

/***/ "./app/app.component.ts":
/*!******************************!*\
  !*** ./app/app.component.ts ***!
  \******************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _opponents_user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./opponents/user */ "./app/opponents/user.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        // Tytuł apki do wyświetlnie w nagłówku strony
        this.title = 'kółko i krzyżyk';
        // Przechowuje informację o tym, czy trwa gra
        this.game = false;
    }
    // Rozpoczyna grę z innym użytkownikiem
    AppComponent.prototype.playWithPlayer = function () {
        this.versus = new _opponents_user__WEBPACK_IMPORTED_MODULE_1__["User"]();
        this.toGame();
    };
    // Rozpoczyna grę z komputerem
    AppComponent.prototype.playWithComputer = function () {
        //this.versus = new Computer();
        //this.toGame();
    };
    // Przerywa grę
    AppComponent.prototype.back = function () {
        this.game = false;
    };
    // Zwraca informację czy trwa gra
    AppComponent.prototype.isGame = function () {
        return this.game;
    };
    // Rozpoczyna grę
    AppComponent.prototype.toGame = function () {
        this.game = true;
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./app/app.module.ts":
/*!***************************!*\
  !*** ./app/app.module.ts ***!
  \***************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "../node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./app/app.component.ts");
/* harmony import */ var _game_game_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./game/game.component */ "./app/game/game.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "../node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/button */ "../node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _field_field_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./field/field.component */ "./app/field/field.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _game_game_component__WEBPACK_IMPORTED_MODULE_3__["GameComponent"],
                _field_field_component__WEBPACK_IMPORTED_MODULE_6__["FieldComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./app/controllers/boardController.ts":
/*!********************************************!*\
  !*** ./app/controllers/boardController.ts ***!
  \********************************************/
/*! exports provided: BoardController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardController", function() { return BoardController; });
/* harmony import */ var _symbols_nullSymbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../symbols/nullSymbol */ "./app/symbols/nullSymbol.ts");

var BoardController = /** @class */ (function () {
    // Inicjalizuje kontroler planszy
    function BoardController(board) {
        this.board = board;
    }
    /*
     Zwraca pole o danych współrzędnych, jeśli nie zostanie znalezione zwraca
     błąd
    */
    BoardController.prototype.getField = function (x, y) {
        var field = null;
        var found = false;
        this.board.forEach(function (tempField) {
            if (tempField.getId() == (x.toString() + y.toString())
                && !found) {
                field = tempField;
                found = true;
            }
        });
        if (field == null) {
            throw new Error("Nie ma takiego pola");
        }
        return field;
    };
    // Resetuje symbole w polach na całej planszy na null symbole
    BoardController.prototype.resetBoard = function () {
        this.board.toArray().forEach(function (field) {
            field.setSymbol(new _symbols_nullSymbol__WEBPACK_IMPORTED_MODULE_0__["NullSymbol"]());
            field.setWin(false);
        });
    };
    /*
      Getters
    */
    BoardController.prototype.getBoard = function () {
        return this.board;
    };
    return BoardController;
}());



/***/ }),

/***/ "./app/controllers/gameController.ts":
/*!*******************************************!*\
  !*** ./app/controllers/gameController.ts ***!
  \*******************************************/
/*! exports provided: GameController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameController", function() { return GameController; });
var GameController = /** @class */ (function () {
    // Inicjalizacja kontrolera planszy
    function GameController(game) {
        this.game = game;
    }
    // Zmiana aktywnego gracza
    GameController.prototype.switchActivePlayer = function () {
        this.game.switchActivePlayer();
    };
    // Zwraca aktywnego gracza
    GameController.prototype.getActivePlayer = function () {
        return this.game.getActivePlayer();
    };
    // Sprawia, że pojawi się przycisk następnej rundy
    GameController.prototype.nextRound = function () {
        this.game.setNextRound(true);
    };
    // Zwraca czy nastąpiła wygrana
    GameController.prototype.wasWon = function () {
        return this.game.isNextRound();
    };
    return GameController;
}());



/***/ }),

/***/ "./app/controllers/winCheckController.ts":
/*!***********************************************!*\
  !*** ./app/controllers/winCheckController.ts ***!
  \***********************************************/
/*! exports provided: WinCheckController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WinCheckController", function() { return WinCheckController; });
var WinCheckController = /** @class */ (function () {
    // Inicjalizuje kontroler do sprawdzenia czy nastąpiła wygrana
    function WinCheckController(gameController) {
        this.gameController = gameController;
    }
    /*
      Sprawdza, czy nastąpiła wygrana, jeśli nastąpiła dodaje zwyciężcy punkt
    */
    WinCheckController.prototype.checkWin = function () {
        var _this = this;
        // Lambda do pobierania symbolu z pola w planszy
        var field = function (x, y) { return _this.boardController.getField(x, y); };
        this.checkFields([field(2, 0), field(1, 1), field(0, 2)]);
        var crossFields = [];
        for (var _i = 0, _a = [0, 1, 2]; _i < _a.length; _i++) {
            var i = _a[_i];
            crossFields.push(field(i, i));
        }
        if (this.checkFields(crossFields)) {
            return true;
        }
        for (var _b = 0, _c = [0, 1, 2]; _b < _c.length; _b++) {
            var i = _c[_b];
            var verticalFields = [];
            var horizontalFields = [];
            for (var _d = 0, _e = [0, 1, 2]; _d < _e.length; _d++) {
                var j = _e[_d];
                verticalFields.push(field(j, i));
                horizontalFields.push(field(i, j));
            }
            if (this.checkFields(verticalFields)
                || this.checkFields(horizontalFields)) {
                return true;
            }
        }
        return false;
    };
    // Sprawdza czy pola w podanej tablicy spełniają warunek wygranej
    WinCheckController.prototype.checkFields = function (fields) {
        if (fields.length != 3) {
            throw new Error("Trzeba podać dokładnie trzy pola do sprawdzenia");
        }
        var symbol = function (field) {
            if (field.isEmpty()) {
                return field.getId();
            }
            return field.getSymbol();
        };
        if (symbol(fields[0]) == symbol(fields[1])
            && symbol(fields[1]) == symbol(fields[2])) {
            fields.forEach(function (field) { return field.setWin(true); });
            this.win();
            return true;
        }
        return false;
    };
    // Dodaje punkt zwycięzcy i aktywuje następną rundę
    WinCheckController.prototype.win = function () {
        this.gameController.getActivePlayer().increaseScore();
        this.gameController.nextRound();
    };
    /*
      Funkcja zwracająca, że nastąpił remis, jeśli na planszy nie ma pustego
      pola
    */
    WinCheckController.prototype.checkDraw = function () {
        var wasEmpty = false;
        this.boardController.getBoard().forEach(function (field) {
            if (field.isEmpty()) {
                wasEmpty = true;
            }
        });
        return !wasEmpty;
    };
    /*
      Setters
    */
    WinCheckController.prototype.setBoardController = function (boardController) {
        this.boardController = boardController;
    };
    return WinCheckController;
}());



/***/ }),

/***/ "./app/field/field.component.css":
/*!***************************************!*\
  !*** ./app/field/field.component.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "button {\r\n  width: 100px;\r\n  height: 100px;\r\n  font-size: 40px;\r\n}"

/***/ }),

/***/ "./app/field/field.component.html":
/*!****************************************!*\
  !*** ./app/field/field.component.html ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button mat-stroked-button color={{getType()}} (click)=\"onClick()\">{{getSymbol()}}</button>\n"

/***/ }),

/***/ "./app/field/field.component.ts":
/*!**************************************!*\
  !*** ./app/field/field.component.ts ***!
  \**************************************/
/*! exports provided: FieldComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FieldComponent", function() { return FieldComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _symbols_nullSymbol__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../symbols/nullSymbol */ "./app/symbols/nullSymbol.ts");
/* harmony import */ var _controllers_gameController__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../controllers/gameController */ "./app/controllers/gameController.ts");
/* harmony import */ var _controllers_winCheckController__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../controllers/winCheckController */ "./app/controllers/winCheckController.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FieldComponent = /** @class */ (function () {
    // Inicjalizuje nowe pole
    function FieldComponent() {
        this.initSymbol();
        this.isWin = false;
    }
    /*
      Metoda wykonywana po kliknięciu w pole, ustawia symbol w polu na symbol
      aktywnego gracza
    */
    FieldComponent.prototype.onClick = function () {
        if (this.isEmpty() && !this.gameController.wasWon()) {
            this.symbol = this.gameController.getActivePlayer()
                .getSymbol();
            if (!this.winChecker.checkWin()) {
                if (this.winChecker.checkDraw()) {
                    this.gameController.nextRound();
                }
            }
            this.gameController.switchActivePlayer();
        }
    };
    // Zwraca typ przycisku w zależności od tego czy jest wygrany, czy nie
    FieldComponent.prototype.getType = function () {
        return this.isWin ? "primary" : "basic";
    };
    // Sprawdza czy pole jest puste
    FieldComponent.prototype.isEmpty = function () {
        return this.getSymbol() == "";
    };
    // Zwraca znak symbolu w polu
    FieldComponent.prototype.getSymbol = function () {
        return this.symbol.symbol();
    };
    // Inicjuje początkową wartość symbolu w danym polu na null
    FieldComponent.prototype.initSymbol = function () {
        this.symbol = new _symbols_nullSymbol__WEBPACK_IMPORTED_MODULE_1__["NullSymbol"]();
    };
    /*
      Getters and setters
    */
    FieldComponent.prototype.setSymbol = function (symbol) {
        this.symbol = symbol;
    };
    FieldComponent.prototype.getId = function () {
        return this.id;
    };
    FieldComponent.prototype.setWin = function (win) {
        this.isWin = win;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], FieldComponent.prototype, "id", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _controllers_gameController__WEBPACK_IMPORTED_MODULE_2__["GameController"])
    ], FieldComponent.prototype, "gameController", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _controllers_winCheckController__WEBPACK_IMPORTED_MODULE_3__["WinCheckController"])
    ], FieldComponent.prototype, "winChecker", void 0);
    FieldComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-field',
            template: __webpack_require__(/*! ./field.component.html */ "./app/field/field.component.html"),
            styles: [__webpack_require__(/*! ./field.component.css */ "./app/field/field.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FieldComponent);
    return FieldComponent;
}());



/***/ }),

/***/ "./app/game/game.component.css":
/*!*************************************!*\
  !*** ./app/game/game.component.css ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n}\r\n\r\n.scores {\r\n  display: flex;\r\n  justify-content: space-around;\r\n}"

/***/ }),

/***/ "./app/game/game.component.html":
/*!**************************************!*\
  !*** ./app/game/game.component.html ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  Grasz z {{versus.name()}}.\n</p>\n<table>\n  <tr *ngFor=\"let i of getGameboardIteration()\">\n    <th *ngFor=\"let j of getGameboardIteration()\">\n      <app-field [id]=\"i.toString()+j.toString()\" [gameController]=\"getController()\" \n        [winChecker]=\"getWinChecker()\"></app-field>\n    </th>\n  </tr>\n</table>\n<br />\n<div class=\"scores\">\r\n  <span><b>Gracz 1({{getPlayer(0).getSymbol().symbol()}}):</b> {{getPlayer(0).getScore()}}</span>\n  <span *ngIf=\"getActivePlayer() == getPlayer(0)\"><b ><</b></span>\n  <span *ngIf=\"getActivePlayer() == getPlayer(1)\"><b>></b></span>\r\n  <span><b>Gracz 2({{getPlayer(1).getSymbol().symbol()}}):</b> {{getPlayer(1).getScore()}}</span>\r\n</div>\n<div *ngIf=\"isNextRound()\">\n  <button mat-button (click)=\"startNextRound()\">Następna runda</button>\n</div>\n"

/***/ }),

/***/ "./app/game/game.component.ts":
/*!************************************!*\
  !*** ./app/game/game.component.ts ***!
  \************************************/
/*! exports provided: GameComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameComponent", function() { return GameComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _opponents_opponent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../opponents/opponent */ "./app/opponents/opponent.ts");
/* harmony import */ var _symbols_xSymbol__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../symbols/xSymbol */ "./app/symbols/xSymbol.ts");
/* harmony import */ var _symbols_oSymbol__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../symbols/oSymbol */ "./app/symbols/oSymbol.ts");
/* harmony import */ var _player__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../player */ "./app/player.ts");
/* harmony import */ var _controllers_gameController__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../controllers/gameController */ "./app/controllers/gameController.ts");
/* harmony import */ var _controllers_winCheckController__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../controllers/winCheckController */ "./app/controllers/winCheckController.ts");
/* harmony import */ var _controllers_boardController__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../controllers/boardController */ "./app/controllers/boardController.ts");
/* harmony import */ var _field_field_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../field/field.component */ "./app/field/field.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var GameComponent = /** @class */ (function () {
    // Inicjalizuje nową grę
    function GameComponent() {
        this.init();
    }
    /*
      Ustawia kontroler planszy dla sprawdzacza po uaktywnieniu się
      @ViewChildren
    */
    GameComponent.prototype.ngAfterViewInit = function () {
        this.boardController = new _controllers_boardController__WEBPACK_IMPORTED_MODULE_7__["BoardController"](this.fields);
        this.winChecker.setBoardController(this.boardController);
    };
    // Aktywuje następną rundę, resetuje planszę
    GameComponent.prototype.startNextRound = function () {
        this.boardController.resetBoard();
        this.nextRound = false;
    };
    // Zmienia aktualnie aktywnego gracza
    GameComponent.prototype.switchActivePlayer = function () {
        this.activePlayer = this.getPlayer(this.getActivePlayer() == this.getPlayer(0) ? 1 : 0);
    };
    // Zwraca gracza o danym indeksie
    GameComponent.prototype.getPlayer = function (i) {
        return this.players[i];
    };
    // Inicjue wartości zmiennych
    GameComponent.prototype.init = function () {
        this.gameboardIteration = [0, 1, 2];
        this.controller = new _controllers_gameController__WEBPACK_IMPORTED_MODULE_5__["GameController"](this);
        this.initPlayers();
        this.drawWhichPlayerStarted();
        this.winChecker = new _controllers_winCheckController__WEBPACK_IMPORTED_MODULE_6__["WinCheckController"](this.controller);
    };
    // Zapełnia tablicę graczami
    GameComponent.prototype.initPlayers = function () {
        this.players = Array(2);
        this.players[0] = new _player__WEBPACK_IMPORTED_MODULE_4__["Player"](new _symbols_xSymbol__WEBPACK_IMPORTED_MODULE_2__["XSymbol"]());
        this.players[1] = new _player__WEBPACK_IMPORTED_MODULE_4__["Player"](new _symbols_oSymbol__WEBPACK_IMPORTED_MODULE_3__["OSymbol"]());
    };
    // Losuje, który gracz ma zaczynać
    GameComponent.prototype.drawWhichPlayerStarted = function () {
        this.activePlayer = this.players[Math.random() >= 0.5 ? 1 : 0];
    };
    /*
      Getters and setters
     */
    GameComponent.prototype.getVersus = function () {
        return this.versus;
    };
    GameComponent.prototype.getGameboardIteration = function () {
        return this.gameboardIteration;
    };
    GameComponent.prototype.getActivePlayer = function () {
        return this.activePlayer;
    };
    GameComponent.prototype.getController = function () {
        return this.controller;
    };
    GameComponent.prototype.getWinChecker = function () {
        return this.winChecker;
    };
    GameComponent.prototype.setNextRound = function (nextRound) {
        this.nextRound = nextRound;
    };
    GameComponent.prototype.isNextRound = function () {
        return this.nextRound;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _opponents_opponent__WEBPACK_IMPORTED_MODULE_1__["Opponent"])
    ], GameComponent.prototype, "versus", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChildren"])(_field_field_component__WEBPACK_IMPORTED_MODULE_8__["FieldComponent"]),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["QueryList"])
    ], GameComponent.prototype, "fields", void 0);
    GameComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-game',
            template: __webpack_require__(/*! ./game.component.html */ "./app/game/game.component.html"),
            styles: [__webpack_require__(/*! ./game.component.css */ "./app/game/game.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], GameComponent);
    return GameComponent;
}());



/***/ }),

/***/ "./app/opponents/opponent.ts":
/*!***********************************!*\
  !*** ./app/opponents/opponent.ts ***!
  \***********************************/
/*! exports provided: Opponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Opponent", function() { return Opponent; });
var Opponent = /** @class */ (function () {
    function Opponent() {
    }
    return Opponent;
}());



/***/ }),

/***/ "./app/opponents/user.ts":
/*!*******************************!*\
  !*** ./app/opponents/user.ts ***!
  \*******************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
/* harmony import */ var _opponent__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./opponent */ "./app/opponents/opponent.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var User = /** @class */ (function (_super) {
    __extends(User, _super);
    function User() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    User.prototype.name = function () {
        return "użytkownikiem";
    };
    User.prototype.isComputer = function () {
        return false;
    };
    return User;
}(_opponent__WEBPACK_IMPORTED_MODULE_0__["Opponent"]));



/***/ }),

/***/ "./app/player.ts":
/*!***********************!*\
  !*** ./app/player.ts ***!
  \***********************/
/*! exports provided: Player */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Player", function() { return Player; });
var Player = /** @class */ (function () {
    // Tworzy nowego gracza używającego podanego znaku
    function Player(symbol) {
        // Wynik gracza
        this.score = 0;
        this.symbol = symbol;
    }
    // Zwiększa wynik gracza o 1
    Player.prototype.increaseScore = function () {
        this.score++;
    };
    /*
      Getters
     */
    Player.prototype.getScore = function () {
        return this.score;
    };
    Player.prototype.getSymbol = function () {
        return this.symbol;
    };
    return Player;
}());



/***/ }),

/***/ "./app/symbols/nullSymbol.ts":
/*!***********************************!*\
  !*** ./app/symbols/nullSymbol.ts ***!
  \***********************************/
/*! exports provided: NullSymbol */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NullSymbol", function() { return NullSymbol; });
/* harmony import */ var _symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./symbol */ "./app/symbols/symbol.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var NullSymbol = /** @class */ (function (_super) {
    __extends(NullSymbol, _super);
    function NullSymbol() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NullSymbol.prototype.symbol = function () {
        return "";
    };
    return NullSymbol;
}(_symbol__WEBPACK_IMPORTED_MODULE_0__["Symbol"]));



/***/ }),

/***/ "./app/symbols/oSymbol.ts":
/*!********************************!*\
  !*** ./app/symbols/oSymbol.ts ***!
  \********************************/
/*! exports provided: OSymbol */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OSymbol", function() { return OSymbol; });
/* harmony import */ var _symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./symbol */ "./app/symbols/symbol.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var OSymbol = /** @class */ (function (_super) {
    __extends(OSymbol, _super);
    function OSymbol() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    OSymbol.prototype.symbol = function () {
        return "o";
    };
    return OSymbol;
}(_symbol__WEBPACK_IMPORTED_MODULE_0__["Symbol"]));



/***/ }),

/***/ "./app/symbols/symbol.ts":
/*!*******************************!*\
  !*** ./app/symbols/symbol.ts ***!
  \*******************************/
/*! exports provided: Symbol */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Symbol", function() { return Symbol; });
var Symbol = /** @class */ (function () {
    function Symbol() {
    }
    return Symbol;
}());



/***/ }),

/***/ "./app/symbols/xSymbol.ts":
/*!********************************!*\
  !*** ./app/symbols/xSymbol.ts ***!
  \********************************/
/*! exports provided: XSymbol */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "XSymbol", function() { return XSymbol; });
/* harmony import */ var _symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./symbol */ "./app/symbols/symbol.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var XSymbol = /** @class */ (function (_super) {
    __extends(XSymbol, _super);
    function XSymbol() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    XSymbol.prototype.symbol = function () {
        return "x";
    };
    return XSymbol;
}(_symbol__WEBPACK_IMPORTED_MODULE_0__["Symbol"]));



/***/ }),

/***/ "./environments/environment.ts":
/*!*************************************!*\
  !*** ./environments/environment.ts ***!
  \*************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./main.ts":
/*!*****************!*\
  !*** ./main.ts ***!
  \*****************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "../node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***********************!*\
  !*** multi ./main.ts ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/daniel/Projects/TicTacToe/client/src/main.ts */"./main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map