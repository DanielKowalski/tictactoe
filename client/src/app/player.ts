import { Symbol } from './symbols/symbol';

export class Player {
    // Wynik gracza
    private score: number;
    // Informacja czy gracz jest komputerem
    private computer: boolean;

    // Tworzy nowego gracza używającego podanego znaku
    constructor(private symbol: Symbol) {
        this.score = 0;
    }

    // Zwiększa wynik gracza o 1
    increaseScore(): void {
        this.score++;
    }

    /*
      Getters and setters
     */

    getScore(): number {
        return this.score;
    }
   
    getSymbol(): Symbol {
        return this.symbol;
    }

    setComputer(computer: boolean): void {
        this.computer = computer;
    }

    isComputer(): boolean {
        return this.computer;
    }
}