import { Component, Input, OnInit } from '@angular/core';
import { Symbol } from '../symbols/symbol';
import { NullSymbol } from '../symbols/nullSymbol';
import { GameController } from '../controllers/gameController';
import { WinCheckController } from '../controllers/winCheckController';
import { ErrorService } from '../services/error.service';
import { AI } from '../controllers/ai';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.css']
})
export class FieldComponent implements OnInit {
    // Symbol, który znajduję się w polu
    private symbol: Symbol;
    // Przechowuje informację, czy pole jest wygrane
    private isWin: boolean;
    // Id pola
    @Input() private id: string;
    // Kontroler gry
    @Input() private gameController: GameController;
    // Kontroler do sprawdzenia, czy nastąpiła wygrana
    @Input() private winChecker: WinCheckController;
    // Logika komputera
    @Input() private ai: AI;

    // Inicjalizuje nowe pole, wstrzykuje sobie serwis do sprawdzenia błędów
    constructor(private errorService: ErrorService) {
        this.initSymbol();
        this.isWin = false;
    }

    // Po zainicjowaniu klasy sprawdza czy podano wymagane parametry
    ngOnInit() {
        this.errorService.checkParameter(this.id, "id");
        this.errorService.checkParameter(this.gameController, "gameController");
        this.errorService.checkParameter(this.winChecker, "winChecker");
        this.errorService.checkParameter(this.ai, "ai");
    }

    /* 
      Metoda wykonywana po kliknięciu w pole, ustawia symbol w polu na symbol
      aktywnego gracza
    */
    onClick(): void {
        if (this.isEmpty() && !this.gameController.wasWon()) {
            this.symbol = this.gameController.getActivePlayer()
                .getSymbol();
            if (!this.winChecker.checkWin()) {
                if (this.winChecker.checkDraw()) {
                    this.gameController.nextRound();
                }
            }
            this.gameController.switchActivePlayer();
            if (this.gameController.getActivePlayer().isComputer()
                && !this.gameController.wasWon()) 
            {
                this.ai.move();
            }
        }
    }

    // Zwraca typ przycisku w zależności od tego czy jest wygrany, czy nie
    getType(): string {
        return this.isWin ? "primary" : "basic";
    } 

    // Sprawdza czy pole jest puste
    isEmpty(): boolean {
        return this.getSymbol() == "";
    }

    // Zwraca znak symbolu w polu
    getSymbol(): string {
        return this.symbol.symbol();
    }

    // Inicjuje początkową wartość symbolu w danym polu na null
    private initSymbol(): void {
        this.symbol = new NullSymbol();
    }

    /*
      Getters and setters
    */

    setSymbol(symbol: Symbol): void {
        this.symbol = symbol;
    }
    

    getId(): string {
        return this.id;
    }

    setWin(win: boolean): void {
        this.isWin = win;
    }
}
