import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GameComponent } from './game/game.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { FieldComponent } from './field/field.component';

@NgModule({
  declarations: [
      AppComponent,
      GameComponent,
      FieldComponent
  ],
  imports: [
      BrowserModule,
      BrowserAnimationsModule,
      MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
