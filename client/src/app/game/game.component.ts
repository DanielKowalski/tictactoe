import { Component, Input, ViewChildren, QueryList, AfterViewInit, 
    AfterViewChecked, 
    ChangeDetectorRef} 
  from '@angular/core';
import { Opponent } from '../opponents/opponent';
import { XSymbol } from '../symbols/xSymbol';
import { OSymbol } from '../symbols/oSymbol';
import { Player } from '../player';
import { GameController } from '../controllers/gameController';
import { WinCheckController } from '../controllers/winCheckController';
import { BoardController } from '../controllers/boardController';
import { FieldComponent } from '../field/field.component';
import { RandomService } from '../services/random.service';
import { AI } from '../controllers/ai';
import { VersusService } from '../services/versus.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements AfterViewInit, AfterViewChecked {
    // Przechowuje wszystkie pola na planszy
    @ViewChildren(FieldComponent) private fields: QueryList<FieldComponent>;
    // Tablica liczb potrzebna do wygenerowania planszy za pomocą *ngFor
    private gameboardIteration: Array<number>;
    // Tablica zawierająca graczy
    private players: Array<Player>;
    // Aktualnie wybierający gracz
    private activePlayer: Player;
    // Kontroler planszy do przekazania dla pól
    private controller: GameController;
    // Kontroler do sprawdzania czy nastąpiła wygrana
    private winChecker: WinCheckController;
    // Przechowuje informację czy wyświetlić przycisk następnej rundy
    private nextRound: boolean;
    // Kontroler planszy
    private boardController: BoardController;
    // Logika komputera
    private ai: AI;

    /* 
      Inicjalizuje nową grę i wstrzykuje sobie serwis do sprawdzania błędów
      i serwis do generowania randomowych wartości  
    */
    constructor(private versus: VersusService,
                private random: RandomService, 
                private changeDetectorRef: ChangeDetectorRef) {
        this.init();
    }

    /*
      Ustawia kontroler planszy dla sprawdzacza po uaktywnieniu się 
      @ViewChildren
    */
    ngAfterViewInit() {
        this.boardController = new BoardController(this.fields);
        this.winChecker.setBoardController(this.boardController);
        this.ai.setBoardController(this.boardController);
        if (this.activePlayer.isComputer()) {
            this.ai.move();
        }
    }

    // Zmienia moment sprawdzania czy wartości zostały zmienione
    ngAfterViewChecked() {
        this.changeDetectorRef.detectChanges();
    }

    // Aktywuje następną rundę, resetuje planszę
    startNextRound():void {
        this.boardController.resetBoard();
        this.nextRound = false;
        if (this.activePlayer.isComputer()) {
            this.ai.move();
        }
    }

    // Zmienia aktualnie aktywnego gracza
    switchActivePlayer(): void {
        this.activePlayer = this.getPlayer(
            this.getActivePlayer() == this.getPlayer(0) ? 1 : 0);
    }

    // Zwraca gracza o danym indeksie
    getPlayer(i: number): Player {
        return this.players[i];
    }

    // Inicjue wartości zmiennych
    private init(): void {
        this.gameboardIteration = [0, 1, 2];
        this.controller = new GameController(this);
        this.ai = new AI(this.controller, this.random);
        this.initPlayers();
        this.drawWhichPlayerStarted();
        this.winChecker = new WinCheckController(this.controller);
    }

    // Zapełnia tablicę graczami
    private initPlayers(): void {
        this.players = Array(2);
        this.players[0] = new Player(new XSymbol());
        this.players[1] = new Player(new OSymbol());
        if (this.versus.getVersus().isComputer()) {
            this.players[this.random.randomNumber(1, 2) - 1].setComputer(true);
        }
    }

    // Losuje, który gracz ma zaczynać
    private drawWhichPlayerStarted(): void {
        this.activePlayer = this.players[this.random.randomNumber(1, 2 ) - 1];
    }

    /*
      Getters and setters
     */

    getVersus(): Opponent {
        return this.versus.getVersus();
    }

    getGameboardIteration(): Array<number> {
        return this.gameboardIteration;
    }

    getActivePlayer(): Player {
        return this.activePlayer;
    }

    getController(): GameController {
        return this.controller;
    }

    getWinChecker(): WinCheckController {
        return this.winChecker;
    }

    setNextRound(nextRound: boolean): void {
        this.nextRound = nextRound;
    }

    isNextRound(): boolean {
        return this.nextRound;
    }

    getAI(): AI {
        return this.ai;
    }
}
