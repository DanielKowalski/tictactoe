import { TestBed } from '@angular/core/testing';

import { VersusService } from './versus.service';

describe('VersusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VersusService = TestBed.get(VersusService);
    expect(service).toBeTruthy();
  });
});
