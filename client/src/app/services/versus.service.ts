import { Injectable } from '@angular/core';
import { Opponent } from '../opponents/opponent';

@Injectable({
  providedIn: 'root'
})
export class VersusService {
    // Przechowuje informację kto jest przeciwnikiem w grze.
    private versus: Opponent;

    constructor() { }

    /*
      Getter and setter
    */ 

    getVersus(): Opponent {
        return this.versus;
    }

    setVersus(versus: Opponent) {
        this.versus = versus;
    }
}
