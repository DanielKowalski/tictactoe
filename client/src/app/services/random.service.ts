import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RandomService {

    constructor() {}

    // Zwraca pseudolosową liczbę z danego przedziału
    randomNumber(start: number, end: number): number {
        return Math.floor(Math.random() * end) + start;
    }

}
