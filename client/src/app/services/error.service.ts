import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

    constructor() {}

    // Jeśli podany parametr jest niezdefiniowany lub jest nullem rzuca błędem
    checkParameter(parameter, name: string): void {
        if (parameter == null || parameter == undefined) {
            throw new Error("Nie podano parametru " + name);
        }
    }
}
