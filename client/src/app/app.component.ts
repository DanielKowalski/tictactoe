import { Component } from '@angular/core';
import { Opponent } from './opponents/opponent';
import { User } from './opponents/user';
import { Computer } from './opponents/computer';
import { VersusService } from './services/versus.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    // Tytuł apki do wyświetlnie w nagłówku strony
    title = 'kółko i krzyżyk';
    // Przechowuje informację o tym, czy trwa gra
    private game = false;

    /* 
      Inicjalizuje apkę, która wstrzykuje sobie serwis do sprawdzania czy 
      przeciwnikiem jest inny użytkownik, czy komputer
    */
    constructor(private versus: VersusService) {}

    // Rozpoczyna grę z innym użytkownikiem
    playWithPlayer(): void {
        this.versus.setVersus(new User());
        this.toGame();
    }

    // Rozpoczyna grę z komputerem
    playWithComputer(): void {
        this.versus.setVersus(new Computer());
        this.toGame();
    }

    // Przerywa grę
    back(): void {
        this.game = false;
    }

    // Rozpoczyna grę
    private toGame(): void {
        this.game = true;
    }

    /*
     Getters
    */

    isGame(): boolean {
        return this.game;
    }
}
