import { BoardController } from "./boardController";
import { RandomService } from "../services/random.service";
import { FieldComponent } from "../field/field.component";
import { GameController } from "./gameController";

export class AI {
    private boardController: BoardController;

    /* 
      Inicjuje nowy obiekt ai, przyjmuje kontroler gry i serwis do generowania 
      pseudolowosych liczb
    */
    constructor(private gameController: GameController, 
                private random: RandomService) {}

    // Komputer wykonuje ruch
    move(): void {
        var aiSymbol: string = this.gameController.getActivePlayer().getSymbol()
            .symbol();
        if (!this.beforeMove(aiSymbol)) {
            if (!this.beforeMove(aiSymbol == 'x' ? 'o' : 'x')) {
                this.randomMove();
            }
        }
    }

    // Komputer daje swój symbol na pseudolosowe miejsce
    private randomMove(): void {
        var fields: Array<FieldComponent> =
            this.boardController.getBoard().toArray();
        var field: FieldComponent;
        do {
            field = fields[this.random.randomNumber(0, fields.length)];
        } while (!field.isEmpty());
        field.onClick();
    }
    
    // Komputer wykonuje ruch w linii, w której są już dwa takie same symoble
    private beforeMove(symbol: string): boolean {
        var field = (x, y) => {
            return this.boardController.getField(x, y);
        }
        if (this.checkFields([field(2, 0), field(1, 1), field(0, 2)], symbol)) {
            return true;
        }
        var crossFields: Array<FieldComponent> = [];
        for (let i of [0, 1, 2]) {
            crossFields.push(field(i, i));
        }
        if (this.checkFields(crossFields, symbol)) {
            return true;         
        }
        for (let i of [0, 1, 2]) {
            var verticalFields: Array<FieldComponent> = [];
            var horizontalFields: Array<FieldComponent> = [];
            for (let j of [0, 1, 2]) {
                verticalFields.push(field(j, i));
                horizontalFields.push(field(i, j));
            }
            if (this.checkFields(verticalFields, symbol) 
                || this.checkFields(horizontalFields, symbol)) 
            {
                return true;
            }
        }
        return false;
    }

    /* 
      Sprawdza czy wśród podanych pól o tym samym znaku jest wolne pole, w 
      którym komputer może postawić swój symbol
    */
    private checkFields(fields: Array<FieldComponent>, symbol: string): boolean {
        if (fields.length != 3) {
            throw new Error("Musisz podać dokładnie trzy pola do sprawdzenia");
        }
        var fieldSymbol = (i) => {
            var field: FieldComponent = fields[i];
            if (field.isEmpty()) {
                return field.getId();
            }
            return field.getSymbol();
        }
        for (let i of [0, 1, 2]) {
            var numbers = [0, 1, 2];
            var index = numbers.indexOf(i);
            numbers.splice(index, 1);
            if (fields[i].isEmpty()) {
                if (fieldSymbol(numbers[0]) == symbol 
                    && fieldSymbol(numbers[1]) == symbol)
                {
                    fields[i].onClick();
                    return true;
                }
            }
        }
        return false;
        /*
        if (fields[0].isEmpty()) {
            if (fieldSymbol(1) == symbol && fieldSymbol(2) == symbol) {
                fields[0].onClick();
            }
        }
        else if (fields[1].isEmpty()) {
            if (fieldSymbol(0) == symbol && fieldSymbol(2) == symbol) {
                fields[1].onClick();
            }
        }
        else if (fields[2].isEmpty()){
            if (fieldSymbol(0) == symbol && fieldSymbol(1) == symbol) {
                fields[2].onClick();
            }
        }
        */
    } 

    /*
      Setters
    */

    setBoardController(boardController: BoardController): void {
        this.boardController = boardController;
    }
}