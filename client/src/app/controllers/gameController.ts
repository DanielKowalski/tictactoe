import { GameComponent } from "../game/game.component";
import { Player } from "../player";

export class GameController {

    // Inicjalizacja kontrolera planszy, przymuję obiekt aktualnej gry
    constructor(private game: GameComponent) {}

    // Zmiana aktywnego gracza
    switchActivePlayer(): void {
        this.game.switchActivePlayer();
    }

    // Zwraca aktywnego gracza
    getActivePlayer(): Player {
        return this.game.getActivePlayer();
    }

    // Sprawia, że pojawi się przycisk następnej rundy
    nextRound(): void {
        this.game.setNextRound(true);
    }

    // Zwraca czy nastąpiła wygrana
    wasWon(): boolean {
        return this.game.isNextRound();
    }
}