import { QueryList } from "@angular/core";
import { FieldComponent } from "../field/field.component";
import { NullSymbol } from "../symbols/nullSymbol";
import { isUndefined } from "util";

export class BoardController {

    // Inicjalizuje kontroler planszy, przyjmuję planszę
    constructor(private board: QueryList<FieldComponent>) {}

    /* 
     Zwraca pole o danych współrzędnych, jeśli nie zostanie znalezione zwraca 
     błąd
    */
    getField(x: number, y: number): FieldComponent {
        var field: FieldComponent = null;
        var found: boolean = false;
        this.board.forEach(tempField => {
            if (tempField.getId() == (x.toString() + y.toString()) 
                && !found) {
                field = tempField;
                found = true;
            }
        });
        if (field == null) {
          throw new Error("Nie ma takiego pola");
        }
        return field;
    }

    // Resetuje symbole w polach na całej planszy na null symbole
    resetBoard(): void {
        this.board.toArray().forEach(field => {
            field.setSymbol(new NullSymbol());
            field.setWin(false);
        });
    }

    /*
      Getters
    */

    getBoard(): QueryList<FieldComponent> {
        return this.board;
    }
}