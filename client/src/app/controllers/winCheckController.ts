import { QueryList } from "@angular/core";
import { GameController } from "./gameController";
import { FieldComponent } from "../field/field.component";
import { BoardController } from "./boardController";

export class WinCheckController {
    // Kontroler planszy
    private boardController: BoardController;

    /* 
      Inicjalizuje kontroler do sprawdzenia czy nastąpiła wygrana, przyjumje 
      kontroler gry  
    */
    constructor(private gameController: GameController) {}

    /* 
      Sprawdza, czy nastąpiła wygrana, jeśli nastąpiła dodaje zwyciężcy punkt
    */
    checkWin(): boolean {
        // Lambda do pobierania symbolu z pola w planszy
        var field = (x, y) => this.boardController.getField(x, y);
         this.checkFields([field(2, 0), field(1, 1), field(0, 2)]);
        var crossFields: Array<FieldComponent> = [];
        for (let i of [0, 1, 2]) {
            crossFields.push(field(i, i));
        }
        if (this.checkFields(crossFields)) {
            return true;
        }
        for (let i of [0, 1, 2]) {
            var verticalFields: Array<FieldComponent> = [];
            var horizontalFields: Array<FieldComponent> = [];
            for (let j of [0, 1, 2]) {
                verticalFields.push(field(j, i));
                horizontalFields.push(field(i, j));
            }
            if (this.checkFields(verticalFields) 
                || this.checkFields(horizontalFields)) 
            {
                return true;
            }
        }
        return false;
    }

    // Sprawdza czy pola w podanej tablicy spełniają warunek wygranej
    checkFields(fields: Array<FieldComponent>): boolean {
        if (fields.length != 3) {
            throw new Error("Trzeba podać dokładnie trzy pola do sprawdzenia");
        }
        var symbol = (field: FieldComponent) => {
            if (field.isEmpty()) {
                return field.getId();
            }
            return field.getSymbol();
        }
        if (symbol(fields[0]) == symbol(fields[1]) 
            && symbol(fields[1]) == symbol(fields[2]))
        {
            fields.forEach(field => field.setWin(true));
            this.win();
            return true;
        }
        return false;
    }

    // Dodaje punkt zwycięzcy i aktywuje następną rundę
    win(): void {
        this.gameController.getActivePlayer().increaseScore();
        this.gameController.nextRound();
    }

    /* 
      Funkcja zwracająca, że nastąpił remis, jeśli na planszy nie ma pustego 
      pola
    */
    checkDraw(): boolean {
        var wasEmpty: boolean = false;
        this.boardController.getBoard().forEach(field => {
            if (field.isEmpty()) {
                wasEmpty = true;
            }
        });
        return !wasEmpty;
    }

    /*
      Setters
    */
   
    setBoardController(boardController: BoardController): void {
        this.boardController = boardController;
    }
}