import { Symbol } from "./symbol";

export class XSymbol extends Symbol {
    symbol() {
        return "x";
    }
}