export abstract class Symbol {
    // Zwraca znak danego symbolu
    abstract symbol(): string;
}