import { Symbol } from './symbol';

export class NullSymbol extends Symbol {
    symbol() {
        return "";
    }
}