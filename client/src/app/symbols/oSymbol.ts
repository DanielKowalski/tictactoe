import { Symbol } from './symbol';

export class OSymbol extends Symbol {
    symbol() {
        return "o";
    }
}