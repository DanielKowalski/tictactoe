import { Opponent } from "./opponent";

export class User extends Opponent {
    // Zwraca informację, że przeciwnik nie jest komputerem
    isComputer(): boolean {
        return false;
    }
}
