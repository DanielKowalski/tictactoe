export abstract class Opponent {
    // Zwraca czy przeciwnik jest komputerem czy nie
    abstract isComputer(): boolean;
}