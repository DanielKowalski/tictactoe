import { Opponent } from "./opponent";

export class Computer extends Opponent {
    // Zwraca informację, że przeciwnik jest komputerem
    isComputer(): boolean {
        return true;
    }
}